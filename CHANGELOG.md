# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Before any major/minor/patch bump all unit tests will be run to verify they pass.

## [Unreleased]

-   [x]

## [0.1.0] - 2024-01-27

-   ignore any packets whose header does match a q3 rcon/query packet.

## [0.0.1] - 2024-01-27

### Added

-   All source files for lilproxy including full commit history.
